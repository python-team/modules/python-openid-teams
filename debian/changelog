python-openid-teams (1.2-4) unstable; urgency=medium

  * Team Upload
  * Use dh-sequence-python3
  * Set Rules-Requires-Root: no
  * Patch-out usage of six

 -- Alexandre Detiste <tchet@debian.org>  Fri, 22 Nov 2024 08:27:22 +0100

python-openid-teams (1.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:07:30 -0400

python-openid-teams (1.2-2) unstable; urgency=medium

  * Team upload.
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 27 Jul 2019 01:58:24 +0200

python-openid-teams (1.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Sergio Durigan Junior ]
  * New upstream version 1.2
  * Update my e-mail address.
  * Bump compat version to 11.
  * Bump Standards-Version to 4.1.5.
  * Update d/copyright.
  * Add Testsuite directive on d/control.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Mon, 23 Jul 2018 02:05:26 -0400

python-openid-teams (1.1-1) unstable; urgency=medium

  * Initial release of python-openid-teams. (Closes: #829655)

 -- Sergio Durigan Junior <sergiodj@sergiodj.net>  Mon, 04 Jul 2016 23:52:44 -0400
